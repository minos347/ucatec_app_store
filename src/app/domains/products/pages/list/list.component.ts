import {Component, signal} from '@angular/core';
import {CommonModule} from "@angular/common";
import {ProductComponent} from "../../components/product/product.component";
import {HeaderComponent} from "../../../shared/components/header/header.component";
import {Product} from "../../../shared/models/product.model";

@Component({
  selector: 'app-list',
  standalone: true,
  imports: [CommonModule, ProductComponent,HeaderComponent],
  templateUrl: './list.component.html',
  styleUrl: './list.component.css'
})
export class ListComponent {

  products = signal<Product[]>([])
  cart = signal<Product[]>([])

  constructor() {
    const initProducts: Product[] = [
      {
        id: Date.now(),
        title:'Audifonos Sony WH-CH720N',
        price: 320,
        image:'/assets/image1.jpg'

      },
      {
        id: Date.now(),
        title:'Audifino Gamer Maxel',
        price: 400,
        image:'/assets/image2.jpg'

      },
      {
        id: Date.now(),
        title:'Audifonos MiSK',
        price: 300,
        image:'/assets/image3.jpg'

      },
      {
        id: Date.now(),
        title:'Audifonos Sony WH-CH510',
        price: 350,
        image:'/assets/image4.jpeg'

      },
      {
        id: Date.now(),
        title:'Audifonos portables',
        price: 180,
        image:'/assets/image5.jpg'

      },
      {
        id: Date.now(),
        title:'Porta audifonos',
        price: 100,
        image:'/assets/image6.jpg'

      },
      {
        id: Date.now(),
        title:'Estuche para audifonos',
        price: 250,
        image:'/assets/image7.jpg'

      },
      {
        id: Date.now(),
        title:'Pedestal para audifonos Gamer',
        price: 150,
        image:'/assets/image8.jpg'

      },
      {
        id: Date.now(),
        title:'Audifonos QYC H2',
        price: 350,
        image:'/assets/image9.jpg'

      },
      {
        id: Date.now(),
        title:'Audifonos Select Sound',
        price: 300,
        image:'/assets/image10.jpeg'

      },
      {
        id: Date.now(),
        title:'Steren audifono',
        price: 150,
        image:'/assets/image11.jpg'

      },
      {
        id: Date.now(),
        title:'Airpod Apple',
        price: 250,
        image:'/assets/image12.jpeg'

      }
    ];
    this.products.set(initProducts);
  }

  addToCart(product: Product){
    this.cart.update(prevState => [...prevState,product]);
  }
}
